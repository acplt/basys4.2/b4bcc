#include <open62541/types.h>  // For UA_DateTime functions

#include <b4b_opMode.h>
#include <b4b_io.h>

typedef struct OpModeContext {
    int workState;
    C3_ES_State exState;
    UA_DateTime timer;
    UA_DateTime preFlushTime;
    UA_DateTime ignitionTime;
    UA_DateTime postFlushTime;
} OpModeContext;

static bool
opMode_init(C3_CC *cc, C3_OP_OpMode *opMode) {
    OpModeContext *context = (OpModeContext *)opMode->context;
    context->workState = -2;
    context->timer = 0;
    return true;
}

static void
opMode_clear(C3_CC *cc, struct C3_OP_OpMode *opMode) {
    free(opMode->context);
}

static bool
opMode_select(C3_CC *cc, struct C3_OP_OpMode *opMode) {
    const C3_Status status = C3_CC_getStatus(cc);
    if(C3_ES_ISACTIVESTATE[status.executionState]) {
        // Don't allow operation mode changes in active states
        // This could be even more permissive (e.g. only change operatin mode in STOPPED)
        return false;
    }

    OpModeContext *context = (OpModeContext *)opMode->context;
    context->exState = -1;
    context->workState = -1;
    context->timer = 0;
    return true;
}

static bool
opMode_deselect(C3_CC *cc, struct C3_OP_OpMode *opMode) {
    const C3_Status status = C3_CC_getStatus(cc);
    if(C3_ES_ISACTIVESTATE[status.executionState]) {
        // Don't allow operation mode changes in active states
        // This could be even more permissive (e.g. only change operatin mode in STOPPED)
        return false;
    }
    return true;
}

static void
opMode_execute(C3_CC *cc, struct C3_OP_OpMode *opMode, C3_IO io, C3_ES_Order *order,
               const char **workingState, const char **errorState) {
    OpModeContext *context = (OpModeContext *)opMode->context;
    B4B_IO *brenner = (B4B_IO *)io;
    C3_Status status = C3_CC_getStatus(cc);
    UA_DateTime now = UA_DateTime_now();

    // Handle an execution state change
    if(status.executionState != context->exState) {
        // Reset state timer and save current state
        context->timer = now;
        context->exState = status.executionState;
        context->workState = 0;
    }

    // Implement execution state behaviour
    if(status.executionState == C3_ES_STATE_RESETTING) {
        if(context->workState == 0) {
            context->workState++;
            *workingState = "Close Valves";
            *errorState = C3_ERRORSTATE_NONE;
        }
        
        brenner->Y105 = false;
        brenner->Y108 = false;
        brenner->Y205open = false;
        brenner->Y205close = true;
        brenner->TZI = false;

        if(brenner->Y205chkclose) {
            *workingState = "Ready";
            if(brenner->P203 == false) {
            *errorState = "Pressure Low";
            *order = C3_ES_ORDER_ABORT;
            }else{
            *order = C3_ES_ORDER_SC;
            }
        }
        

    } else if(status.executionState == C3_ES_STATE_STARTING) {
        if(context->workState == 0) {
            context->workState++;
            *workingState = "Open Y205";
        }
        if(context->workState == 1) {
            //Vorspülung warten auf Y205 chkbck
            brenner->Y105 = false;
            brenner->Y108 = false;
            brenner->Y205open = true;
            brenner->Y205close = false;
            if(brenner->Y205chkopen) {
                context->workState++;
                context->timer = now;
                *workingState = "Preflushing";
            }
        }
        if(context->workState == 2) {
            //Vorspülung            
            if(now > (context->timer + context->preFlushTime)) {
                context->workState++;
                context->timer = now;
                *workingState = "Air Valve Adjustment";
            }
        }
        if(context->workState == 3) {
            //Zündung
            brenner->Y205open  = false;
            brenner->Y205close = true;
            if(brenner->Y205chksetpoint) {
                brenner->Y205open  = false;
                brenner->Y205close = false;
                context->workState++;
                context->timer = now;
                *workingState = "Ignition";
            }

        } 
        if(context->workState == 4) {
            if(brenner->P103 == false) {
                *errorState = "Pressure Low";
                *order = C3_ES_ORDER_ABORT;
            }
            if(brenner->P104 == false) {
                *errorState = "Pressure High";
                *order = C3_ES_ORDER_ABORT;
            }
            brenner->TZI = true;
            brenner->Y105 = true;
            brenner->Y108 = true;
            if(brenner->F130) {
                brenner->TZI = false;
                *order = C3_ES_ORDER_SC;
            }else if(now > (context->timer + context->ignitionTime))  {
                brenner->TZI = false;
                if(brenner->P103 == false && brenner->P107 == false) {
                    *errorState = "Pressure Low";
                    *order = C3_ES_ORDER_ABORT;
                }else{
                    *errorState = "Timeout";
                    *order = C3_ES_ORDER_ABORT;
                }
            }
        }
    } else if(status.executionState == C3_ES_STATE_EXECUTE) {
        if(context->workState == 0) {
            context->workState++;
            *workingState = "Burning";
        }
        if((brenner->P103 && brenner->P107  && brenner->P203) == false) {
                *errorState = "Pressure Low";
                *order = C3_ES_ORDER_ABORT;
        }
        if(brenner->P104 == false) {
                *errorState = "Pressure High";
                *order = C3_ES_ORDER_ABORT;
        }
        if(brenner->F130 == false) {
                *errorState = "Flame out";
                *order = C3_ES_ORDER_ABORT;
        }
    } else if(status.executionState == C3_ES_STATE_STOPPING){
        if(context->workState == 0) {
            context->workState++;
            *workingState = "Open Y205";
        }
        brenner->TZI = false;
        if(context->workState == 1) {
            //Nachspülung warten auf Y205 chkbck
            if(brenner->P203 == false) {
                *errorState = "Pressure Low";
                *order = C3_ES_ORDER_ABORT;
            }
            brenner->Y105 = false;
            brenner->Y108 = false;
            brenner->Y205open = true;
            brenner->Y205close = false;
            if(brenner->Y205chkopen) {
                context->workState++;
                context->timer = now;
                *workingState = "Postflushing";
            }
        }
        if(context->workState == 2) {
            //Nachspülung
            if(now > (context->timer + context->postFlushTime)) {
                context->workState++;
                context->timer = now;
                *workingState = "Close Valves";
            }
        }
        if(context->workState == 3) {
            //Ventileschlißen
            brenner->Y205open = false;
            brenner->Y205close = true;
            if(brenner->Y205chkclose) {
                *order = C3_ES_ORDER_SC;
            }
        } 
    } else if(status.executionState == C3_ES_STATE_CLEARING) {
       if(context->workState == 0) {
            context->workState++;
            *workingState = "Open Y205";
        }
        brenner->TZI = false;
        if(context->workState == 1) {
            //Nachspülung warten auf Y205 chkbck
            if(brenner->P203 == false) {
                *errorState = "Pressure Low";
                *order = C3_ES_ORDER_ABORT;
            }
            brenner->Y105 = false;
            brenner->Y108 = false;
            brenner->Y205open = true;
            brenner->Y205close = false;
            if(brenner->Y205chkopen) {
                context->workState++;
                context->timer = now;
                *workingState = "Postflushing";
            }
        }
        if(context->workState == 2) {
            //Nachspülung
            if(now > (context->timer + context->postFlushTime)) {
                context->workState++;
                context->timer = now;
                *workingState = "Close Valves";
            }
        }
        if(context->workState == 3) {
            //Ventileschlißen
            brenner->Y205open = false;
            brenner->Y205close = true;
            if(brenner->Y205chkclose) {
                *order = C3_ES_ORDER_SC;
            }
        } 
    } else if(status.executionState == C3_ES_STATE_ABORTING){
        if(context->workState == 0) {
            context->workState++;
            *workingState = "Close Valves";
        }
        if(context->workState == 1) {
            //Ventileschlißen
            brenner->TZI = false;
            brenner->Y105 = false;
            brenner->Y108 = false;
            brenner->Y205open = false;
            brenner->Y205close = true;
            if(brenner->Y205chkclose) {
                *order = C3_ES_ORDER_SC;
            }
        }
    } else if(C3_ES_ISACTIVESTATE[status.executionState]) {
        // An active state, that was not covered / implemented (e.g. HOLDING, UNHOLDING)
        // --> Just skip through
        *workingState = "NotImplemented";
        *errorState = "NotImplemented";
        *order = C3_ES_ORDER_SC;
    }
}

bool
b4b_opMode_add(C3_CC *cc) {
    OpModeContext *context = malloc(sizeof(OpModeContext));
    if(context == NULL)
        return false;

    context->preFlushTime = UA_DATETIME_SEC * 5;
    context->postFlushTime = UA_DATETIME_SEC * 5;
    context->ignitionTime = UA_DATETIME_SEC * 8;
    //  Notfallziet= t#3s

    C3_OP_OpMode opMode;
    opMode.context = context;
    opMode.name = "Burn";
    opMode.init = opMode_init;
    opMode.execute = opMode_execute;
    opMode.clear = opMode_clear;
    opMode.select = opMode_select;
    opMode.deselect = opMode_deselect;
    if(C3_CC_addOperationMode(cc, &opMode) < 0) {
        free(context);
    }
    return true;
}