#include <b4b_io.h>
#ifdef B4BCC_ENABLE_REVPI
#include <b4b_revpi.h>
#endif

#include <open62541/types.h>  // For UA_DateTime functions

UA_DateTime timerTZI;
UA_DateTime timerY205;
B4B_IO B4B_IO_SIM = {0};
bool B4B_IO_OVERWRITESIMFROMUA = true;
static long int ioCounter = 0;

#ifdef B4BCC_ENABLE_REVPI
static struct GpioHandle revPi_DI[8] = {};
static struct outputProcessState* revPi_DO[8] = {};
#endif

static void
io_init(void *context, C3_IO *io) {
    *io = calloc(1, sizeof(B4B_IO));
    B4B_IO_SIM = (B4B_IO){0};
    timerTZI = 0;
    timerY205 = 0;
#ifdef B4BCC_ENABLE_REVPI
    for (size_t i = 0; i < 8; i++) {
        revPi_DI[i] = open_din(i);
        revPi_DO[i] = createoutputUpdateThread(i);
    }
#endif
}

#ifdef B4BCC_ENABLE_REVPI
static void
io_clear(void *context) {
    for (size_t i = 0; i < 8; i++) {
        close_gpios(revPi_DI[i]);
        setOutputProcessState(revPi_DO[i], false);
        stopoutputUpdateThread(revPi_DO[i]);
    }
}

static void
io_read(void *context, C3_IO io) {
    B4B_IO *brenner = (B4B_IO *)io;
    // brenner->P103 = read_din(revPi_DI[0]);
    // brenner->P104 = read_din(revPi_DI[1]);
    // brenner->P107 = read_din(revPi_DI[2]);
    // brenner->P203 = read_din(revPi_DI[3]);
    // brenner->Y205chkopen = read_din(revPi_DI[4]);
    // brenner->Y205chkclose = read_din(revPi_DI[5]);
    // brenner->F130 = read_din(revPi_DI[6]);
    // brenner->Y205chksetpoint = read_din(revPi_DI[7]);
    brenner->P103 = B4B_IO_SIM.P103;
    brenner->P104 = B4B_IO_SIM.P104;
    brenner->P107 = B4B_IO_SIM.P107;
    brenner->P203 = B4B_IO_SIM.P203;
    brenner->Y205chkopen = B4B_IO_SIM.Y205chkopen;
    brenner->Y205chkclose = B4B_IO_SIM.Y205chkclose;
    brenner->F130 = B4B_IO_SIM.F130;
    brenner->Y205chksetpoint = B4B_IO_SIM.Y205chksetpoint;
}


static void
io_write(void *context, C3_IO io) {
    B4B_IO *brenner = (B4B_IO *)io;
    setOutputProcessState(revPi_DO[0], brenner->Y105);
    setOutputProcessState(revPi_DO[1], brenner->Y108);
    setOutputProcessState(revPi_DO[2], brenner->Y205open);
    setOutputProcessState(revPi_DO[3], brenner->Y205close);
    setOutputProcessState(revPi_DO[4], brenner->TZI);
    setOutputProcessState(revPi_DO[5], false);
    setOutputProcessState(revPi_DO[6], false);
    setOutputProcessState(revPi_DO[7], false);
}
#endif

static void
io_readSim(void *context, C3_IO io) {
    B4B_IO *brenner = (B4B_IO *)io;

    //Simulate Burner behaviour if not overwritten
    if(!B4B_IO_OVERWRITESIMFROMUA) {
        UA_DateTime now = UA_DateTime_now();
        // Simulate Y205 behaviour
        if(B4B_IO_SIM.Y205open && !B4B_IO_SIM.Y205close && !B4B_IO_SIM.Y205chkopen) {
            B4B_IO_SIM.Y205chkclose = false;
            if(timerY205 == 0) {
                timerY205 = now + (2 * UA_DATETIME_SEC);
            }else if(now > timerY205) {
                B4B_IO_SIM.Y205chkopen = true;
                timerY205 = 0;
            }
        }
        if(B4B_IO_SIM.Y205close && !B4B_IO_SIM.Y205open && !B4B_IO_SIM.Y205chkclose) {
            B4B_IO_SIM.Y205chkopen = false;
            if(timerY205 == 0) {
                timerY205 = now + (2 * UA_DATETIME_SEC);
            }else if(now > timerY205) {
                B4B_IO_SIM.Y205chkclose = true;
                timerY205 = 0;
            }
        }
        
        // Simulate simplified burner behaviour: activate F130 after 2sec
        if(B4B_IO_SIM.TZI && B4B_IO_SIM.Y205chksetpoint && B4B_IO_SIM.Y105 && B4B_IO_SIM.Y108) {
            if(timerTZI  == 0) {
                timerTZI = UA_DateTime_now() + (2 * UA_DATETIME_SEC);
            }else if(now > timerTZI) {
                B4B_IO_SIM.F130 = true;
            }
        }
        else if(!B4B_IO_SIM.Y205chksetpoint || !B4B_IO_SIM.Y105 || !B4B_IO_SIM.Y108) {
            B4B_IO_SIM.F130 = false;
            timerTZI = 0;
        }
    }
    
    //Check if input changed
    if(B4B_IO_SIM.P103 != brenner->P103 ||
       B4B_IO_SIM.P104 != brenner->P104 ||
       B4B_IO_SIM.P107 != brenner->P107 ||
       B4B_IO_SIM.P203 != brenner->P203 ||
       B4B_IO_SIM.Y205chksetpoint != brenner->Y205chksetpoint ||
       B4B_IO_SIM.Y205chkopen != brenner->Y205chkopen ||
       B4B_IO_SIM.Y205chkclose != brenner->Y205chkclose ||
       B4B_IO_SIM.F130 != brenner->F130 ||
       B4B_IO_SIM.P103 != brenner->P103) {
        printf(" - Simulated inputs changed:\tP103=%d, P104=%d, P107=%d, P203=%d, Y205chkopen=%d, Y205chkclose=%d,Y205chksetpoint=%d, F130=%d\n",
                B4B_IO_SIM.P103,
                B4B_IO_SIM.P104,
                B4B_IO_SIM.P107,
                B4B_IO_SIM.P203,
                B4B_IO_SIM.Y205chkopen,
                B4B_IO_SIM.Y205chkclose,
                B4B_IO_SIM.Y205chksetpoint,
                B4B_IO_SIM.F130
        );
    }
    if(B4B_IO_SIM.Y105 != brenner->Y105 ||
       B4B_IO_SIM.Y108 != brenner->Y108 ||
       B4B_IO_SIM.Y205open != brenner->Y205open ||
       B4B_IO_SIM.Y205close != brenner->Y205close ||
       B4B_IO_SIM.TZI != brenner->TZI) {
        printf(" - Simulated outputs changed:\tY105=%d, Y108=%d, Y205open=%d, Y205close=%d, TZI=%d\n",
                B4B_IO_SIM.Y105,
                B4B_IO_SIM.Y108,
                B4B_IO_SIM.Y205open,
                B4B_IO_SIM.Y205close,
                // YO 205_6 ?
                B4B_IO_SIM.TZI 
        );
    }
    *brenner = B4B_IO_SIM;
}

static void
io_writeSim(void *context, C3_IO io) {
    B4B_IO *brenner = (B4B_IO *)io;
    if(B4B_IO_SIM.Y105 != brenner->Y105 ||
       B4B_IO_SIM.Y108 != brenner->Y108 ||
       B4B_IO_SIM.Y205open != brenner->Y205open ||
       B4B_IO_SIM.Y205close != brenner->Y205close ||
       B4B_IO_SIM.TZI != brenner->TZI) {
        printf(" - Write simulated outputs:\tY105=%d, Y108=%d, Y205open=%d, Y205close=%d, TZI=%d\n",
                brenner->Y105,
                brenner->Y108,
                brenner->Y205open,
                brenner->Y205close,
                // YO 205_6 ?
                brenner->TZI
        );
        B4B_IO_SIM.Y105 = brenner->Y105;
        B4B_IO_SIM.Y108 = brenner->Y108;
        B4B_IO_SIM.Y205open = brenner->Y205open;
        B4B_IO_SIM.Y205close = brenner->Y205close;
        B4B_IO_SIM.TZI = brenner->TZI;
    }
}

static void
io_interlock(void *context, C3_IO io) {
    B4B_IO *brenner = (B4B_IO *)io;
}

void
b4b_io_add(C3_CC *cc) {
    C3_IOConfig ioConfig = C3_IOCONFIG_NULL;
    ioConfig.init = io_init;
#ifdef B4BCC_ENABLE_REVPI
    ioConfig.clear = io_clear;
    ioConfig.read = io_read;
    ioConfig.write = io_write;
#endif
    ioConfig.readSim = io_readSim;
    ioConfig.writeSim = io_writeSim;
    ioConfig.interlock = io_interlock;
    C3_CC_setIOConfig(cc, ioConfig);
}