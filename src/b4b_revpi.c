#include "b4b_revpi.h"

#include <stdio.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

static struct gpiod_chip *
find_chip(const char *label) {
    size_t NUM_CHIPS = 5;
    //TODO use „/dev/gpiochip_din“ und „/dev/gpiochip_dout“ instead?
    char **CHIPS = (char *[]){"/dev/gpiochip0", "/dev/gpiochip1", "/dev/gpiochip2", "/dev/gpiochip3", "/dev/gpiochip4"};

    for(size_t i = 0; i < NUM_CHIPS; ++i) {
        struct gpiod_chip *chip = gpiod_chip_open(CHIPS[i]);
        printf("%i:%s\n", i, gpiod_chip_label(chip));
        if(strcmp(gpiod_chip_label(chip), label) == 0) {
            return chip;
        }
        gpiod_chip_close(chip);
    }
}

struct GpioHandle
open_din(u_int8_t pin) {
    struct GpioHandle handle;
    handle.chip = find_chip("max31913");
    handle.line = gpiod_chip_get_line(handle.chip, pin);
    gpiod_line_request_input(handle.line, "b4b");
    return handle;
}

struct GpioHandle
open_dout(u_int8_t pin) {
    struct GpioHandle handle;
    handle.chip = find_chip("74hc595");
    handle.line = gpiod_chip_get_line(handle.chip, pin);
    gpiod_line_request_output(handle.line, "b4b", 0);
    return handle;
}

bool
read_din(struct GpioHandle handle) {
    return (bool)gpiod_line_get_value(handle.line);
}
void
write_dout(struct GpioHandle handle, bool value) {
    gpiod_line_set_value(handle.line, value);
}

void
close_gpios(struct GpioHandle handle) {
    gpiod_line_release(handle.line);
    gpiod_chip_close(handle.chip);
}

static void *
outputProcessStateMain(void *data) {
    struct outputProcessState *sharedState = (struct outputProcessState *)data;
    pthread_mutex_lock(&sharedState->lock);
    while(!sharedState->stop) {
        write_dout(sharedState->gpioHandle, sharedState->value);
        pthread_mutex_unlock(&sharedState->lock);
        usleep(5000);
        pthread_mutex_lock(&sharedState->lock);
    }
    close_gpios(sharedState->gpioHandle);
    return NULL;
}

struct outputProcessState*
createoutputUpdateThread(int pin) {
    struct outputProcessState* result = malloc(sizeof(struct outputProcessState));
    result->stop = false;
    result->value = false;
    pthread_mutex_init(&result->lock, NULL);
    result->gpioHandle = open_dout(pin);
    pthread_create(&result->thread, NULL, &outputProcessStateMain, result);
    return result;
}

void
setOutputProcessState(struct outputProcessState *state, bool value) {
    pthread_mutex_lock(&state->lock);
    state->value = value;
    pthread_mutex_unlock(&state->lock);
}

bool
getOutputProcessState(struct outputProcessState *state) {
    pthread_mutex_lock(&state->lock);
    bool value = state->value;
    pthread_mutex_unlock(&state->lock);
    return value;
}

void
stopoutputUpdateThread(struct outputProcessState *state) {
    pthread_mutex_lock(&state->lock);
    state->stop = true;
    pthread_join(state->thread, NULL);
    pthread_mutex_unlock(&state->lock);
    pthread_mutex_destroy(&state->lock);
    free(state);
}