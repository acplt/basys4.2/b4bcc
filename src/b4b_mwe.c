#include <cc_connector.h>
#include <cc_instanciation.h>

#include <profile_utilities.h>

#include <signal.h>
#include <stdio.h>
#include <stdlib.h>

#include <b4b_opMode.h>
#include <b4b_io.h>

UA_Boolean running = true;
static void
stopHandler(int sig) {
    running = false;
}

static void
usage(void) {
    // TODO add possibility to read information from config
    printf("Usage: b4b_mwe");
    printf(" [--profile <name>|<csv>]");
    printf(" [--port <port>]");
    printf(" [--optional]");
    printf(" [--help]\n");
}
static void
usageError(const char *errorMsg) {
    fprintf(stderr, "Error: %s\n", errorMsg);
    usage();
}

static int
parseCLI(int argc, char **argv, C3_Profile *profile, UA_UInt16 *port,
         bool *createOptional) {
    for(int argpos = 1; argpos < argc; argpos++) {
        if(strcmp(argv[argpos], "--help") == 0 || strcmp(argv[argpos], "-h") == 0) {
            usage();
            printf(
                "\n\tExample: b4b_mwe --profile BASYS --port 16664 --optional\n");
            printf("\tProfile parameter:  Available profiles are BASYSDEMO, BASYS, "
                   "PACKML, FULL, "
                   "FULLOPTIONAL.\n\t\tOr custom profiles in the format "
                   "'SI,OC,EM,ES,OM,optional', "
                   "e.g. '4,6,6,2,2,0' for BASYS profile.\n");
            return EXIT_FAILURE;
        }
        // parse server url
        if(strcmp(argv[argpos], "--port") == 0) {
            argpos++;
            if(argpos >= argc) {
                usageError("parameter --port given, but no port specified!");
                return EXIT_FAILURE;
            }
            *port = strtol(argv[argpos], NULL, 10);
            continue;
        }
        if(strcmp(argv[argpos], "--profile") == 0 || strcmp(argv[argpos], "-p") == 0) {
            argpos++;
            if(argpos >= argc) {
                usageError("parameter --profile given, but no profile specified!");
                return EXIT_FAILURE;
            }
            *profile = C3_Profile_parse(argv[argpos]);
            if(C3_Profile_isEqual(*profile, C3_PROFILE_NULL)) {
                usageError("parameter --profile given, but no valid profile specified! "
                           "Available profiles are BASYSDEMO, BASYS, PACKML, FULL, "
                           "FULLOPTIONAL. Or "
                           "custom profiles in the format 'SI,OC,EM,ES,OM,optional', "
                           "e.g. '4,6,6,2,2,0' for BASYS profile.");
                return EXIT_FAILURE;
            }
            continue;
        }
        // parse create optional nodes
        if(strcmp(argv[argpos], "--optional") == 0 || strcmp(argv[argpos], "-o") == 0) {
            *createOptional = true;
            continue;
        }
        // TODO add option to read CreateAllOptionalNodes?
        // TODO add option to specify example
        // Unknown option
        usageError("unknown command line option.");
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

static void
createBaSys4BrennerControlComponent(UA_Server *server, C3_Profile profile) {
    /* Create control component and fill in example informations */
    C3_CC *cc = C3_CC_new();
    C3_Info info;
    info.id = "Burner";
    info.name = "BaSys4Brenner Burner";
    info.description = "Represents the control component for a BaSys4Brenner burner.";
    info.profile = profile;
    info.type = "B4BBurner";
    C3_CC_setInfo(cc, &info);

    b4b_io_add(cc);

    b4b_opMode_add(cc);

    UA_NodeId typeId = UA_NODEID_NUMERIC(NS_DSTYPES, 1);
    UA_NodeId id = UA_NODEID_NUMERIC(NS_APPLICATION, 1);
    createControlComponentType(server, cc, typeId);
    createControlComponent(server, cc, typeId, id);
}

static UA_StatusCode
readSimIO(UA_Server *server, const UA_NodeId *sessionId, void *sessionContext, const UA_NodeId *nodeId, void *nodeContext,
          UA_Boolean sourceTimeStamp, const UA_NumericRange *range, UA_DataValue *dataValue) {
    UA_StatusCode result = UA_Variant_setScalarCopy(&dataValue->value, nodeContext, &UA_TYPES[UA_TYPES_BOOLEAN]);
    dataValue->hasValue = true;
    return result;
}

static UA_StatusCode
writeSimIO(UA_Server *server, const UA_NodeId *sessionId, void *sessionContext, const UA_NodeId *nodeId, void *nodeContext,
           const UA_NumericRange *range, const UA_DataValue *data) {
    if(B4B_IO_OVERWRITESIMFROMUA || (nodeContext == &B4B_IO_OVERWRITESIMFROMUA)) {
        *((bool*)(nodeContext)) = *((bool*)(data->value.data));
        return UA_STATUSCODE_GOOD;
    }
    return UA_STATUSCODE_BADUSERACCESSDENIED;
}

static void
addSimVariable(UA_Server *server, UA_UInt32 id, char* browsename, char* displayname, bool* variable) {
    UA_VariableAttributes attr = UA_VariableAttributes_default;
    attr.displayName = UA_LOCALIZEDTEXT("en-US", displayname);
    attr.accessLevel = UA_ACCESSLEVELMASK_READ | UA_ACCESSLEVELMASK_WRITE;

    UA_DataSource simIODataSource;
    simIODataSource.read = readSimIO;
    simIODataSource.write = writeSimIO;
    UA_Server_addDataSourceVariableNode(server, UA_NODEID_NUMERIC(NS_APPLICATION, id), UA_NODEID_NUMERIC(NS_APPLICATION, 200000),
                                        UA_NODEID_NUMERIC(0, UA_NS0ID_HASPROPERTY), UA_QUALIFIEDNAME(NS_APPLICATION, browsename),
                                        UA_NODEID_NUMERIC(0, UA_NS0ID_BASEDATAVARIABLETYPE), attr,
                                        simIODataSource, variable, NULL);
}

static void addSimVariablesToUA(UA_Server *server) {
    UA_UInt32 id = 200000;
    addFolder(server, "SIMIO", "Simulated IOs", "Folder for the simulated IOs of the BaSys4Brenner Burner",
              UA_NODEID_NUMERIC(NS_APPLICATION, id), UA_NODEID_NUMERIC(0, UA_NS0ID_OBJECTSFOLDER), false);
    addSimVariable(server, ++id, "OVERWRITESIMFROMUA", "OverwriteSimulatedIOFromUA", &B4B_IO_OVERWRITESIMFROMUA);

    addSimVariable(server, ++id, "P103", "P103", &B4B_IO_SIM.P103);
    addSimVariable(server, ++id, "P104", "P104", &B4B_IO_SIM.P104);
    addSimVariable(server, ++id, "P107", "P107", &B4B_IO_SIM.P107);
    addSimVariable(server, ++id, "P203", "P203", &B4B_IO_SIM.P203);
    addSimVariable(server, ++id, "Y205chkopen", "Y205chkopen", &B4B_IO_SIM.Y205chkopen);    
    addSimVariable(server, ++id, "Y205chkclose", "Y205chkclose", &B4B_IO_SIM.Y205chkclose);
    addSimVariable(server, ++id, "Y205chksetpoint", "Y205chksetpoint", &B4B_IO_SIM.Y205chksetpoint);
    addSimVariable(server, ++id, "Y205open", "Y205open", &B4B_IO_SIM.Y205open);
    addSimVariable(server, ++id, "Y205close", "Y205close", &B4B_IO_SIM.Y205close);
    addSimVariable(server, ++id, "Y105", "Y105", &B4B_IO_SIM.Y105);
    addSimVariable(server, ++id, "Y108", "Y108", &B4B_IO_SIM.Y108);
    addSimVariable(server, ++id, "TZI", "TZI", &B4B_IO_SIM.TZI);
    addSimVariable(server, ++id, "F130", "F130", &B4B_IO_SIM.F130);
}

int
main(int argc, char **argv) {
    signal(SIGINT, stopHandler);
    signal(SIGTERM, stopHandler);

    /* Parse command line arguments */
    // TODO use regex or CLI library instead: http://tclap.sf.net/ or
    // http://optionparser.sourceforge.net/index.html
    C3_Profile profile = C3_PROFILE_UNKNOWN;
    UA_UInt16 port = 4840;
    bool createOptional = false;
    if(parseCLI(argc, argv, &profile, &port, &createOptional) != EXIT_SUCCESS)
        return EXIT_FAILURE;
    // Define default profile
    if(C3_Profile_isUnknown(profile)) {
        profile.SI = C3_SI_CMD | C3_SI_OPERATIONS;
        profile.OC = C3_OC_NONE;
        profile.EM = C3_EM_AUTO | C3_EM_SIMULATE;
        profile.ES = C3_ES_BASYS;
        profile.OM = C3_OM_NONE;
    }
    // Overwrite optional if specified
    profile.optional = profile.optional || createOptional;

    /* Create an empty server with default settings */
    UA_Server *server = UA_Server_new();
    /* Customize server name and ns1 name */
    // TODO make loglevel a cli parameter
    setServerConfig(server, "BaSys4Brenner Minimum Working Example", "b4b_mwe", port, UA_LOGLEVEL_INFO);

    // Create list of simulated IO variables to simulate the burner
    addSimVariablesToUA(server);

    // Create types and parent folder for dummy CCs
    createControlComponentEnvironment(server, NS_PROFILES_URI, "BrennerherstellerURL");

    createBaSys4BrennerControlComponent(server, profile);

    /* Run the control components */
    void *cc_executionLoopContext = cc_executionLoopContext_new(server);
    UA_UInt64 cc_executionLoopId = 1;
    UA_Server_addRepeatedCallback(server, cc_executionLoop, cc_executionLoopContext, B4B_CC_CYCLETIME, &cc_executionLoopId);

    /* Run the server */
    UA_StatusCode retval = UA_Server_run(server, &running);

    /* Free ressources */
    UA_Server_removeCallback(server, cc_executionLoopId);
    cc_executionLoopContext_delete(cc_executionLoopContext);
    clearControlComponentEnvironment(server);
    UA_Server_delete(server);
    return retval == UA_STATUSCODE_GOOD ? EXIT_SUCCESS : EXIT_FAILURE;
}