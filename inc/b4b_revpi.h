#ifndef B4B_REVPI_H
#define B4B_REVPI_H

#include <gpiod.h>
#include <pthread.h>

struct GpioHandle {
    struct gpiod_chip *chip;
    struct gpiod_line *line;
};

struct GpioHandle
open_din(u_int8_t pin);
struct GpioHandle
open_dout(u_int8_t pin);
bool
read_din(struct GpioHandle handle);
void
write_dout(struct GpioHandle handle, bool value);
void
close_gpios(struct GpioHandle handle);

struct outputProcessState {
    bool value;
    bool stop;
    pthread_mutex_t lock;
    pthread_t thread;
    struct GpioHandle gpioHandle;
};

struct outputProcessState*
createoutputUpdateThread(int pin);
void
setOutputProcessState(struct outputProcessState *state, bool value);
bool
getOutputProcessState(struct outputProcessState *state);
void
stopoutputUpdateThread(struct outputProcessState *state);

#endif /* B4B_REVPI_H */