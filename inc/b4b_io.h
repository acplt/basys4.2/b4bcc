#ifndef B4B_IO_H
#define B4B_IO_H

#include <C3_ControlComponent.h>

// Define Control Component Cycletime in ms
#define B4B_CC_CYCLETIME 5

/* Define Brenner IO*/
typedef struct B4B_IO {
    // Hard wired buttons?
    // bool B_start; //st_0
    // bool B_stop; //st_1
    // bool B_abort; //E_0
    // bool B_reset; //R_0
    // bool B_clear; //C_0
    // PZL
    bool P103;
    bool P104;
    bool P107;
    bool P203;
    // Valves
    bool Y205chkopen;
    bool Y205chkclose;
    bool Y205chksetpoint;

    bool Y205open;
    bool Y205close;
    //TODO: YO 205_6 ?
    bool Y105;
    bool Y108;
    // Zündtrafo
    bool TZI;
    // Flame detection
    bool F130;
} B4B_IO;

// Static Simulated IOs
extern B4B_IO B4B_IO_SIM;
extern bool B4B_IO_OVERWRITESIMFROMUA;

// Adds the io callbacks to the control component
void
b4b_io_add(C3_CC *cc);

#endif /* B4B_IO_H */