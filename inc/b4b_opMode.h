#ifndef B4B_OPMODE_H
#define B4B_OPMODE_H

#include <C3_ControlComponent.h>

// Adds the dummy operation mode to a control component
bool
b4b_opMode_add(C3_CC *cc);

#endif /* B4B_OPMODE_H */