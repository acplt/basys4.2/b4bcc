# BaSys 4 Brenner Control Component Implementation

This project implements a control component as minimum working example for the BaSys4Brenner in C project.

The project uses:

* the [C3 library](https://git.rwth-aachen.de/acplt/basys4.2/c3/) as control component C implementation library
* the [CCProfilesUA](https://git.rwth-aachen.de/acplt/basys4.2/ccProfilesUA/) profile server to generate OPC UA interfaces
* the [open62541 SDK](https://www.open62541.org) for the OPC UA Server
* the [gpiod library](https://git.kernel.org/pub/scm/libs/libgpiod/libgpiod.git) to read and write IOs on a [Revolution Pi](https://revolutionpi.de/tutorials/uebersicht-revpi-compact/)

These are integrated as submodules via the CCProfilesUA project.

## Download

Latest builds on main:

* [Minimum Working Example - Linux 32bit (armhf) for Revolution Pi](https://git.rwth-aachen.de/acplt/basys4.2/b4bcc/-/jobs/artifacts/main/download?job=build%3Amwe%3Arevpi)
* [Minimum Working Example - Windows 64bit](https://git.rwth-aachen.de/acplt/basys4.2/b4bcc/-/jobs/artifacts/main/download?job=build%3Amwe%3Awin64)
* [Minimum Working Example - Linux 64bit](https://git.rwth-aachen.de/acplt/basys4.2/b4bcc/-/jobs/artifacts/main/download?job=build%3Amwe%3Alinux64)
* [Compliance Test Tool - Linux 64bit](https://git.rwth-aachen.de/acplt/basys4.2/b4bcc/-/jobs/artifacts/main/download?job=build%3Atests%3Alinux64)

 [![pipeline status](https://git.rwth-aachen.de/acplt/basys4.2/b4bcc/badges/main/pipeline.svg)](https://git.rwth-aachen.de/acplt/basys4.2/b4bcc/-/commits/main)

## Use

Start executable from shell: `bin/b4b_mwe` or `bin\b4b_mwe.exe` on windows.

Parameters: `b4b_mwe [--profile <name>|<csv>] [--port <port>] [--optional] [--help]`

Example: `b4b_mwe --profile BASYS --port 16664 --optional`

Profile parameter:  Available profiles are BASYSDEMO, BASYS, PACKML, FULL, FULLOPTIONAL. Or custom profiles in the format 'SI,OC,EM,ES,OM,optional', e.g. '4,6,6,2,2,0' for BASYS profile. The PACKML profile is not compiled on default, to reduce the binary size and speed.

## Compile

Here are some hints how to build the executable from linux build host.
The shell commands need to be executed from within the repository folder.
Make sure to initialize the submodules before: `git submodule update --init --recursive`

To compile on revolution pi do:

```console
mkdir build
cd ../build
cmake -DB4BCC_ENABLE_REVPI_IO=ON ..
```

To cross-compile for revolution pi from linux or WSL with static libgpiod from submodule do):

```console
mkdir build
cd libgpiod
./autogen.sh --build=x86_64-linux-gnu --host=arm-linux-gnueabihf --prefix=$(pwd)/../build --enable-shared=no ac_cv_func_malloc_0_nonnull=yes
make
make install
cd ../build
cmake -DB4BCC_ENABLE_REVPI_IO=ON -DB4BCC_USE_LOCAL_LIBGPIOD=ON -DCMAKE_TOOLCHAIN_FILE=../cmake/gcc-linux-armhf.cmake ..
make
```

To compile on linux or WSL with simulation mode only (no reading/writing of IOs in AUTO execution mode) do:

```console
mkdir build
cd ../build
cmake ..
make
```

To cross-compile for windows from linux or WSL with simulation mode only (no reading/writing of IOs in AUTO execution mode) do:

```console
mkdir build
cd build
cmake -DCMAKE_TOOLCHAIN_FILE=../ccProfilesUA/cmake/Toolchain-x86_64-w64-mingw32.cmake ..
make
```

## Open ToDos:

* Test cross-compiled executable on revolution pi (with libgpiod v1.2.1)
* Delete overwrite of DI signals from OPC UA in [src/b4b_io.c]() in `io_read(...)`
* Check ignition time (currently 8s)
* Deactivate piControl on RevPi permanently: [piControl deaktivieren](https://revolutionpi.de/tutorials/ein-ausgaenge-steuern-ohne-picontrol-compact/)
